//
// Created by neethan on 2021/2/20.
//

#include <linux/module.h>

#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/miscdevice.h>
#include <linux/kernel.h>
#include <linux/major.h>
#include <linux/mutex.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/stat.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/tty.h>
#include <linux/kmod.h>
#include <linux/gfp.h>

/* 1. 确定主设备号                                                                 */
static int major = 0;
static char kernel_buf[1024];
static struct class *hello_class;

#define MIN(a, b) (a < b ? a : b)

/* 3. 实现对应的open/read/write等函数，填入file_operations结构体                   */
static ssize_t hello_read (struct file *file, char __user *buf, size_t size, loff_t *offset)
{
int err;
printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
err = copy_to_user(buf, kernel_buf, MIN(1024, size));
return MIN(1024, size);
}

static ssize_t hello_write (struct file *file, const char __user *buf, size_t size, loff_t *offset)
{
int err;
printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
err = copy_from_user(kernel_buf, buf, MIN(1024, size));
return MIN(1024, size);
}

static int hello_open (struct inode *node, struct file *file)
{
    printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
    return 0;
}

static int hello_close (struct inode *node, struct file *file)
{
    printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
    return 0;
}

/* 2. 定义自己的file_operations结构体                                              */
static struct file_operations hello_fops = {
        .owner	 = THIS_MODULE,
        .open    = hello_open,
        .read    = hello_read,
        .write   = hello_write,
        .release = hello_close,
};

static int __init hello_init(void)
{
    int err = 0;
    major = register_chrdev(0, "hello", &hello_fops);  /* /dev/hello */

    // 内核同时提供了class_create(…)函数，可以用它来创建一个类，这个类存放于sysfs下面，
    // 一旦创建好了这个类，再调用device_create(…)函数来在/dev目录下创建相应的设备节点。
    // 这样，加载模块的时候，用户空间中的udev会自动响应device_create(…)函数，去/sysfs下寻找对应的类从而创建设备节点。
    hello_class = class_create(THIS_MODULE, "hello_class");
    err = PTR_ERR(hello_class);
    if (IS_ERR(hello_class)) {
        printk("%s %s line %d, err=%d\n", __FILE__, __FUNCTION__, __LINE__, err);
        unregister_chrdev(major, "hello");
        return -1;
    }

    device_create(hello_class, NULL, MKDEV(major, 0), NULL, "hello"); /* /dev/hello */
    printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
    return 0;
}


static void __exit hello_exit(void)
{
    device_destroy(hello_class, MKDEV(major, 0));
    class_destroy(hello_class);
    unregister_chrdev(major, "hello");
    printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");