#include <linux/init.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <sound/core.h>
#include <linux/spi/spi.h>
#include <asm/uaccess.h>
#include <linux/timer.h>
#include <linux/gpio.h>
#include <linux/mtd/cfi.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/ioport.h>
#include <linux/vmalloc.h>
#include <linux/mtd/mtdram.h>
#include <linux/err.h>
#include <linux/errno.h>
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#include <linux/mtd/spi-nor.h>
#include <linux/delay.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_gpio.h>

/* 参考:
 * drivers/mtd/devices/mtdram.c
 * drivers/mtd/devices/m25p80.c
 */


#define W25X_WriteEnable		      0x06 
#define W25X_WriteDisable		      0x04 
#define W25X_ReadStatusReg		      0x05 
#define W25X_WriteStatusReg		      0x01 
#define W25X_ReadData			      0x03 
#define W25X_FastReadData		      0x0B 
#define W25X_FastReadDual		      0x3B 
#define W25X_PageProgram		      0x02 
#define W25X_BlockErase			      0xD8 
#define W25X_SectorErase		      0x20 
#define W25X_ChipErase			      0xC7 
#define W25X_PowerDown			      0xB9 
#define W25X_ReleasePowerDown	      0xAB 
#define W25X_DeviceID			      0xAB 
#define W25X_ManufactDeviceID   	  0x90 
#define W25X_JedecDeviceID		      0x9F 

#define WIP_Flag                      0x01  /* Write In Progress (WIP) flag */

#define Dummy_Byte                    0xFF


static struct spi_device *spi_flash;


void SPIFlashReadID(int *pMID, int *pDID)
{
    unsigned char tx_buf[4];
    unsigned char rx_buf[2];
    
    tx_buf[0] = W25X_ManufactDeviceID;
    tx_buf[1] = Dummy_Byte;
    tx_buf[2] = Dummy_Byte;
    tx_buf[3] = 0;

    gpio_set_value(spi_flash->cs_gpio, 0);
    spi_write_then_read(spi_flash, tx_buf, 4, rx_buf, 2);
    gpio_set_value(spi_flash->cs_gpio, 1);

    *pMID = rx_buf[0];
    *pDID = rx_buf[1];
}

void SPI_FLASH_ReadDeviceID(void)
{
    unsigned char tx_buf[4];
    unsigned char rx_buf[2];
    
    tx_buf[0] = W25X_DeviceID;
    tx_buf[1] = Dummy_Byte;
    tx_buf[2] = Dummy_Byte;
    tx_buf[3] = Dummy_Byte;

    gpio_set_value(spi_flash->cs_gpio, 0);
    spi_write_then_read(spi_flash, tx_buf, 4, rx_buf, 1);
    gpio_set_value(spi_flash->cs_gpio, 1);

    printk("%s, W25X_DeviceID:0x%x\n", __FUNCTION__, rx_buf[0]);
}

static void SPIFlashWriteEnable(int enable)
{
    unsigned char val = enable ? 0x06 : 0x04;

    gpio_set_value(spi_flash->cs_gpio, 0);
    spi_write(spi_flash, &val, 1);   
    gpio_set_value(spi_flash->cs_gpio, 1); 
}

static unsigned char SPIFlashReadStatusReg1(void)
{
    unsigned char val;
    unsigned char cmd = 0x05;

    gpio_set_value(spi_flash->cs_gpio, 0);
    spi_write_then_read(spi_flash, &cmd, 1, &val, 1);
    gpio_set_value(spi_flash->cs_gpio, 1);

    return val;
}

static unsigned char SPIFlashReadStatusReg2(void)
{
    unsigned char val;
    unsigned char cmd = 0x35;

    gpio_set_value(spi_flash->cs_gpio, 0);
    spi_write_then_read(spi_flash, &cmd, 1, &val, 1);
    gpio_set_value(spi_flash->cs_gpio, 1);
    return val;
}

static void SPIFlashWaitWhenBusy(void)
{
    while (SPIFlashReadStatusReg1() & 1)
    {
        /* 休眠一段时间 */
        /* Sector erase time : 60ms
         * Page program time : 0.7ms
         * Write status reg time : 10ms
         */
		set_current_state(TASK_INTERRUPTIBLE);
        schedule_timeout(HZ/100);  /* 休眠10MS后再次判断 */
    }
}

static void SPIFlashWriteStatusReg(unsigned char reg1, unsigned char reg2)
{    
    unsigned char tx_buf[4];

    SPIFlashWriteEnable(1);  
    
    tx_buf[0] = 0x01;
    tx_buf[1] = reg1;
    tx_buf[2] = reg2;
    gpio_set_value(spi_flash->cs_gpio, 0);
    spi_write(spi_flash, tx_buf, 3);   
    gpio_set_value(spi_flash->cs_gpio, 1);

    SPIFlashWaitWhenBusy();
}

static void SPIFlashClearProtectForStatusReg(void)
{
    unsigned char reg1, reg2;

    reg1 = SPIFlashReadStatusReg1();
    reg2 = SPIFlashReadStatusReg2();

    reg1 &= ~(1<<7);
    reg2 &= ~(1<<0);

    SPIFlashWriteStatusReg(reg1, reg2);
}


static void SPIFlashClearProtectForData(void)
{
    /* cmp=0,bp2,1,0=0b000 */
    unsigned char reg1, reg2;

    reg1 = SPIFlashReadStatusReg1();
    reg2 = SPIFlashReadStatusReg2();

    reg1 &= ~(7<<2);
    reg2 &= ~(1<<6);

    SPIFlashWriteStatusReg(reg1, reg2);
}

/* erase 4K */
void SPIFlashEraseSector(unsigned int addr)
{
    unsigned char tx_buf[4];
    tx_buf[0] = 0x20;
    tx_buf[1] = addr >> 16;
    tx_buf[2] = addr >> 8;
    tx_buf[3] = addr & 0xff;

    SPIFlashWriteEnable(1);  
    gpio_set_value(spi_flash->cs_gpio, 0);
    spi_write(spi_flash, tx_buf, 4);
    gpio_set_value(spi_flash->cs_gpio, 1);
    SPIFlashWaitWhenBusy();
}

/* program */
void SPIFlashProgram(unsigned int addr, unsigned char *buf, int len)
{
    unsigned char tx_buf[4];   
	struct spi_transfer	t[] = {
            {
    			.tx_buf		= tx_buf,
    			.len		= 4,
        	},
			{
    			.tx_buf		= buf,
    			.len		= len,
			},
		};
	struct spi_message	m;

    tx_buf[0] = 0x02;
    tx_buf[1] = addr >> 16;
    tx_buf[2] = addr >> 8;
    tx_buf[3] = addr & 0xff;

    SPIFlashWriteEnable(1);  

	spi_message_init(&m);
	spi_message_add_tail(&t[0], &m);
	spi_message_add_tail(&t[1], &m);
	spi_sync(spi_flash, &m);

    SPIFlashWaitWhenBusy();    
}

void SPIFlashRead(unsigned int addr, unsigned char *buf, int len)
{
    /* spi_write_then_read规定了tx_cnt+rx_cnt < 32
     * 所以对于大量数据的读取，不能使用该函数
     */
     
    unsigned char tx_buf[4];   
	struct spi_transfer	t[] = {
            {
    			.tx_buf		= tx_buf,
    			.len		= 4,
        	},
			{
    			.rx_buf		= buf,
    			.len		= len,
			},
		};
	struct spi_message	m;

    tx_buf[0] = 0x03;
    tx_buf[1] = addr >> 16;
    tx_buf[2] = addr >> 8;
    tx_buf[3] = addr & 0xff;

	spi_message_init(&m);
	spi_message_add_tail(&t[0], &m);
	spi_message_add_tail(&t[1], &m);
	spi_sync(spi_flash, &m);    
}


static void SPIFlashInit(void)
{
    SPIFlashClearProtectForStatusReg();
    SPIFlashClearProtectForData();
}

/* 构造注册一个mtd_info
 * mtd_device_register(master, parts, nr_parts)
 *
 */


/* 首先: 构造注册spi_driver
 * 然后: 在spi_driver的probe函数里构造注册mtd_info
 */

static struct mtd_info spi_flash_dev;



static int spi_flash_erase(struct mtd_info *mtd, struct erase_info *instr)
{
    unsigned int addr = instr->addr;
    unsigned int len  = 0;

    printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);

    /* 判断参数 */
    if ((addr & (spi_flash_dev.erasesize - 1)) || (instr->len & (spi_flash_dev.erasesize - 1)))
    {
        printk("addr/len is not aligned\n");
        return -EINVAL;
    }

    for (len = 0; len < instr->len; len += 4096)
    {
        SPIFlashEraseSector(addr);
        addr += 4096;
    }
    
	instr->state = MTD_ERASE_DONE;
	mtd_erase_callback(instr);
	return 0;
}

static int spi_flash_read2(struct mtd_info *mtd, loff_t from, size_t len,
		size_t *retlen, u_char *buf)
{
    printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
    SPIFlashRead(from, buf, len);
	*retlen = len;
	return 0;
}

static int spi_flash_write(struct mtd_info *mtd, loff_t to, size_t len,
		size_t *retlen, const u_char *buf)
{
    unsigned int addr = to;
    unsigned int wlen  = 0;
    printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);

    /* 判断参数 */
    if ((to & (spi_flash_dev.erasesize - 1)) || (len & (spi_flash_dev.erasesize - 1)))
    {
        printk("addr/len is not aligned\n");
        return -EINVAL;
    }

    for (wlen = 0; wlen < len; wlen += 256)
    {
        SPIFlashProgram(addr, (unsigned char *)buf, 256);
        addr += 256;
        buf += 256;
    }

	*retlen = len;
	return 0;
}
 



static int sr_spidev_probe(struct spi_device *spi)
{		
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
    int mid, did;
    int ret = 0;
    struct device_node	*of_node;

    spi_flash = spi;
    spi->mode = SPI_MODE_0;
    spi_setup(spi);

    memset(&spi_flash_dev, 0, sizeof(spi_flash_dev));

	/* Setup the MTD structure */
	spi_flash_dev.name = "spi_flash";
	spi_flash_dev.type = MTD_NORFLASH;
	spi_flash_dev.flags = MTD_CAP_NORFLASH;
	spi_flash_dev.size = 0x800000;  /* 8MB */
	spi_flash_dev.writesize = 1;
	spi_flash_dev.writebufsize = 4096; /* 没有用到 */
	spi_flash_dev.erasesize = 4096; /* 擦除的最小单位 */

	spi_flash_dev.owner = THIS_MODULE;
	spi_flash_dev._erase = spi_flash_erase;
	spi_flash_dev._read = spi_flash_read2;
	spi_flash_dev._write = spi_flash_write;

    // /* 获取片选 */
    of_node = of_get_parent(spi->dev.of_node);
    spi->cs_gpio = of_get_named_gpio(of_node, "cs-gpios", 0);
    ret = gpio_request(spi->cs_gpio, "cs");
    if(ret < 0){
        printk("error, %s gpio_request spi->cs_gpio %d, ret=%d\n", __FUNCTION__, spi->cs_gpio, ret);
        return -1;
    }
    gpio_direction_output(spi->cs_gpio, 1);
    
    SPIFlashInit();
    SPIFlashReadID(&mid, &did);
    printk("SPI Flash ID: %02x %02x\n", mid, did);

    SPI_FLASH_ReadDeviceID();

    /* 构造注册一个mtd_info
     * mtd_device_register(master, parts, nr_parts)
     *
     */
    ret = mtd_device_register(&spi_flash_dev, NULL, 0);

    return ret; 
}


static int sr_spidev_remove(struct spi_device *spi)
{
    printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
    mtd_device_unregister(&spi_flash_dev);
    gpio_free(spi_flash->cs_gpio);
    return 0;
}

static const struct of_device_id spi_flash_of_match[] = {
	{ .compatible = "sr, spidev", },
	{ },
};
MODULE_DEVICE_TABLE(of, spi_flash_of_match);


/* 1. 定义spi_driver */
static struct spi_driver spi_flash_drv = {
    .driver     = {
        .owner	= THIS_MODULE,
        .name   = "spi_flash",
        .of_match_table = spi_flash_of_match,
    },
    .probe      = sr_spidev_probe,
    .remove     = sr_spidev_remove,
};

/* 2. 在入口函数注册platform_driver */
static int __init sr_spi_flash_init(void)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	
	return spi_register_driver(&spi_flash_drv);
}

/* 3. 有入口函数就应该有出口函数：卸载驱动程序时，就会去调用这个出口函数
 *     卸载platform_driver
 */
static void __exit sr_spi_flash_exit(void)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
    spi_unregister_driver(&spi_flash_drv);
}


/* 7. 其他完善：提供设备信息，自动创建设备节点                                     */

module_init(sr_spi_flash_init);
module_exit(sr_spi_flash_exit);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Neethan <neethan@foxmail.com>");
MODULE_DESCRIPTION("sr spidev for srtos");