#include <linux/bitops.h>
#include <linux/gpio/driver.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#include <linux/seq_file.h>
#include <linux/spi/spi.h>
#include <linux/regmap.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/miscdevice.h>
#include <linux/major.h>
#include <linux/proc_fs.h>
#include <linux/stat.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/tty.h>
#include <linux/kmod.h>
#include <linux/gfp.h>

/* 构造注册 spi_driver */

static int major;
static struct class *class;
static struct spi_device *spi_oled;
static unsigned char *ker_buf = NULL;


#define OLED_CMD_INIT           0x100001
#define OLED_CMD_CLEAR_ALL      0x100002
#define OLED_CMD_CLEAR_PAGE     0x100003
#define OLED_CMD_SET_POS        0x100004

#define OLED_DC_PF22_PIN        250


static void OLED_Set_DC(char val)
{
    gpio_set_value(OLED_DC_PF22_PIN, val);
}

static void OLEDWriteCmd(unsigned char cmd)
{
    OLED_Set_DC(0); /* command */
    gpio_set_value(spi_oled->cs_gpio, 0);
    spi_write(spi_oled, &cmd, 1);
    gpio_set_value(spi_oled->cs_gpio, 1);
    OLED_Set_DC(1); /*  */
}

static void OLEDWriteDat(unsigned char dat)
{
    OLED_Set_DC(1); /* data */
    gpio_set_value(spi_oled->cs_gpio, 0);
    spi_write(spi_oled, &dat, 1);
    gpio_set_value(spi_oled->cs_gpio, 1);
    OLED_Set_DC(1); /*  */
}

static void OLEDSetPageAddrMode(void)
{
    OLEDWriteCmd(0x20);
    OLEDWriteCmd(0x02);
}

static void OLEDSetPos(int page, int col)
{
    OLEDWriteCmd(0xB0 + page); /* page address */

    OLEDWriteCmd(col & 0xf);   /* Lower Column Start Address */
    OLEDWriteCmd(0x10 + (col >> 4));   /* Lower Higher Start Address */
}


static void OLEDClear(void)
{
    int page, i;
    for (page = 0; page < 8; page ++)
    {
        OLEDSetPos(page, 0);
        for (i = 0; i < 128; i++)
            OLEDWriteDat(0);
    }
}

void OLEDClearPage(int page)
{
    int i;
    OLEDSetPos(page, 0);
    for (i = 0; i < 128; i++)
        OLEDWriteDat(0);    
}

void OLEDInit(void)
{
    /* 向OLED发命令以初始化 */
    OLEDWriteCmd(0xAE); /*display off*/ 
    OLEDWriteCmd(0x00); /*set lower column address*/ 
    OLEDWriteCmd(0x10); /*set higher column address*/ 
    OLEDWriteCmd(0x40); /*set display start line*/ 
    OLEDWriteCmd(0xB0); /*set page address*/ 
    OLEDWriteCmd(0x81); /*contract control*/ 
    OLEDWriteCmd(0x66); /*128*/ 
    OLEDWriteCmd(0xA1); /*set segment remap*/ 
    OLEDWriteCmd(0xA6); /*normal / reverse*/ 
    OLEDWriteCmd(0xA8); /*multiplex ratio*/ 
    OLEDWriteCmd(0x3F); /*duty = 1/64*/ 
    OLEDWriteCmd(0xC8); /*Com scan direction*/ 
    OLEDWriteCmd(0xD3); /*set display offset*/ 
    OLEDWriteCmd(0x00); 
    OLEDWriteCmd(0xD5); /*set osc division*/ 
    OLEDWriteCmd(0x80); 
    OLEDWriteCmd(0xD9); /*set pre-charge period*/ 
    OLEDWriteCmd(0x1f); 
    OLEDWriteCmd(0xDA); /*set COM pins*/ 
    OLEDWriteCmd(0x12); 
    OLEDWriteCmd(0xdb); /*set vcomh*/ 
    OLEDWriteCmd(0x30); 
    OLEDWriteCmd(0x8d); /*set charge pump enable*/ 
    OLEDWriteCmd(0x14); 

    OLEDSetPageAddrMode();

    OLEDClear();
    
    OLEDWriteCmd(0xAF); /*display ON*/    
}


static ssize_t oled_write(struct file *file, const char __user *buffer,
                     size_t count, loff_t *pos)
{
    if(count > 4096) return -EINVAL;

    copy_from_user(ker_buf, buffer, count);

    gpio_set_value(spi_oled->cs_gpio, 0);
    spi_write(spi_oled, ker_buf, count);
    gpio_set_value(spi_oled->cs_gpio, 1);
    return 0;
}



static long oled_ioctl(struct file *file,
		unsigned int cmd, unsigned long arg)
{
	long ret;
    int page;
    int col;

    switch(cmd){
        case OLED_CMD_INIT:
        {
            OLEDInit();
            break;
        }
         case OLED_CMD_CLEAR_ALL:
        {
            OLEDClear();
            break;
        }
        case OLED_CMD_CLEAR_PAGE:
        {
            page = arg;
            OLEDClearPage(page);
            break;
        }
        case OLED_CMD_SET_POS:
        {
            page = arg&0xff;
            col = (arg>>8)&0xff;
            OLEDSetPos(page, col);
            break;
        }
        
        default:
            break;
    }
	return ret;
}

static struct file_operations oled_fops = {
    .owner      = THIS_MODULE,
    .write      = oled_write,
    .unlocked_ioctl = oled_ioctl,
}; 

static int spi_oled_probe(struct spi_device *spi)
{
    int32_t ret = 0;
    struct device_node	*of_node;

    spi_oled = spi;

    ker_buf = kmalloc(4096, GFP_KERNEL);

    if(!gpio_is_valid(OLED_DC_PF22_PIN)) {
        printk("error, %s %d gpio_is_valid\n", __FILE__, __LINE__);
    }
    ret = gpio_request(OLED_DC_PF22_PIN, "oled_dc");
    if(ret < 0) {
        printk("error, %s %d gpio_request\n", __FILE__, __LINE__);
    }
    //gpio_direction_input(OLED_DC_PF22_PIN);
    gpio_direction_output(OLED_DC_PF22_PIN,1);


     /* 获取片选 */
    of_node = of_get_parent(spi->dev.of_node);
    spi->cs_gpio = of_get_named_gpio(of_node, "cs-gpios", 0);
    ret = gpio_request(spi->cs_gpio, "cs");
    if(ret < 0){
        printk("error, %s gpio_request cs_gpio %d, ret=%d\n", __FUNCTION__, spi->cs_gpio, ret);
        return -1;
    }
    gpio_direction_output(spi->cs_gpio, 1);

    /* 注册一个file_operations */
    major = register_chrdev(0, "oled", &oled_fops);
    
    class = class_create(THIS_MODULE, "oled");

    /* 为了让mdev根据这些信息来创建设备节点 */
    device_create(class, NULL, MKDEV(major, 0), NULL, "oled"); // /dev/oled
	return 0;
}

static int spi_oled_remove(struct spi_device *spi)
{
    gpio_free(OLED_DC_PF22_PIN);
    gpio_free(spi_oled->cs_gpio);
    kfree(ker_buf);
    device_destroy(class, MKDEV(major, 0));
    class_destroy(class);
    unregister_chrdev(major, "oled");

    return 0;
}

static const struct spi_device_id spi_oled_ids[] = {
	{ "spi_oled" },
	{},
};
MODULE_DEVICE_TABLE(spi, spi_oled_ids);

static const struct of_device_id spi_oled_of_match[] = {
	{ .compatible = "sr, spi_oled" },
	{},
};
MODULE_DEVICE_TABLE(of, spi_oled_of_match);

static struct spi_driver spi_oled_driver = {
	.probe    = spi_oled_probe,
    .remove   = spi_oled_remove,
	.id_table = spi_oled_ids,
	.driver   = {
		.name           = "spi_oled",
        .owner          = THIS_MODULE,
		.of_match_table = of_match_ptr(spi_oled_of_match),
	},
};

static int spi_oled_init(void)
{
    return spi_register_driver(&spi_oled_driver);
}

static void spi_oled_exit(void)
{
    spi_unregister_driver(&spi_oled_driver);
}

module_init(spi_oled_init);
module_exit(spi_oled_exit);

MODULE_AUTHOR("Neethan <neethan@foxmail.com>");
MODULE_DESCRIPTION("spi oled for gui");
MODULE_LICENSE("GPL v2");