//
// Created by neethan on 2021/2/21.
//

#include <linux/module.h>

#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/miscdevice.h>
#include <linux/kernel.h>
#include <linux/major.h>
#include <linux/mutex.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/stat.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/tty.h>
#include <linux/kmod.h>
#include <linux/gfp.h>
#include <linux/gpio/consumer.h>
#include <linux/platform_device.h>


/* 1. 确定主设备号                                                                 */
static int major = 0;
static struct class *led_class;
static struct gpio_desc *led_gpio;

/* 3. 实现对应的open/read/write等函数，填入file_operations结构体                   */
static ssize_t led_drv_read (struct file *file, char __user *buf, size_t size, loff_t *offset)
{
    int err;
    char status;

    status = gpiod_get_value(led_gpio);
    err = copy_to_user(buf, &status, 1);
    printk("%s %s line %d, status:%d\n", __FILE__, __FUNCTION__, __LINE__, status);

    return 1;
}

/* write(fd, &val, 1); */
static ssize_t led_drv_write (struct file *file, const char __user *buf, size_t size, loff_t *offset)
{
    int err;
    char status;
    //struct inode *inode = file_inode(file);
    //int minor = iminor(inode);

    err = copy_from_user(&status, buf, 1);
    /* 根据次设备号和status控制LED */
    gpiod_set_value(led_gpio, status);
    printk("%s %s line %d, status:%d\n", __FILE__, __FUNCTION__, __LINE__, status);

    return 1;
}

static int led_drv_open (struct inode *node, struct file *file)
{
    //int minor = iminor(node);

    printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
    /* 根据次设备号初始化LED */
    gpiod_direction_output(led_gpio, 0);

    return 0;
}

static int led_drv_close (struct inode *node, struct file *file)
{
    printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
    return 0;
}

/* 定义自己的file_operations结构体                                              */
static struct file_operations led_drv = {
        .owner	 = THIS_MODULE,
        .open    = led_drv_open,
        .read    = led_drv_read,
        .write   = led_drv_write,
        .release = led_drv_close,
};

static const struct of_device_id gpio_leds_of_match[] = {
        { .compatible = "100ask,leddrv" },
        { },
};
MODULE_DEVICE_TABLE(of, gpio_leds_of_match);

/* 4. 从platform_device获得GPIO
 *    把file_operations结构体告诉内核：注册驱动程序
 */
static int gpio_leds_probe(struct platform_device *pdev)
{
    int i, error;

    /* 4.1 设备树中定义有: led-gpios=<...>;	*/
    led_gpio = gpiod_get(&pdev->dev, "led", 0);
    if (IS_ERR(led_gpio)){
        dev_err(&pdev->dev, "Failed to get GPIO for led\n");
        return PTR_ERR(led_gpio);
    }

    /* 4.2 注册file_operations 	*/
    major = register_chrdev(0, "100ask_led", &led_drv);  /* /dev/led */

    led_class = class_create(THIS_MODULE, "100ask_led_class");
    if (IS_ERR(led_class)) {
        printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
        unregister_chrdev(major, "led");
        gpiod_put(led_gpio);
        return PTR_ERR(led_class);
    }

    device_create(led_class, NULL, MKDEV(major, 0), NULL, "100ask_led%d", 0); /* /dev/100ask_led0 */
    printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);

    return 0;
}

static struct platform_driver gpio_leds_device_driver = {
        .probe		= gpio_leds_probe,
        .driver		= {
                .name	= "100ask_led",
                .of_match_table = gpio_leds_of_match,
        }
};

static int __init gpio_leds_init(void)
{
    return platform_driver_register(&gpio_leds_device_driver);
}

static void __exit gpio_leds_exit(void)
{
    platform_driver_unregister(&gpio_leds_device_driver);
}

module_init(gpio_leds_init);
module_exit(gpio_leds_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Neethan <neethan@foxmail.com>");
MODULE_DESCRIPTION("led driver for GPIOs");