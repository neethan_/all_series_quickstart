#include <linux/module.h>

#include <linux/init.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/slab.h>
#include <linux/sysctl.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/input.h>
#include <linux/gpio_keys.h>
#include <linux/workqueue.h>
#include <linux/gpio.h>
#include <linux/gpio/consumer.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/spinlock.h>



static int gpio_keys_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	size_t size;
	int i, error;


	return 0;
}


static int gpio_keys_remove(struct platform_device *pdev)
{

    return 0;
}
		


static const struct of_device_id gpio_keys_of_match[] = {
	{ .compatible = "sr, gpio-keys", },
	{ },
};
MODULE_DEVICE_TABLE(of, gpio_keys_of_match);

static struct platform_driver gpio_keys_device_driver = {
	.probe		= gpio_keys_probe,
    .remove     = gpio_keys_remove,
	.driver		= {
		.name	= "sr_gpio_keys",
		.of_match_table = gpio_keys_of_match,
	}
};

static int __init gpio_keys_init(void)
{
	return platform_driver_register(&gpio_keys_device_driver);
}

static void __exit gpio_keys_exit(void)
{
	platform_driver_unregister(&gpio_keys_device_driver);
}

late_initcall(gpio_keys_init);
module_exit(gpio_keys_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Neethan <neethan@foxmail.com>");
MODULE_DESCRIPTION("sr key driver for GPIOs");